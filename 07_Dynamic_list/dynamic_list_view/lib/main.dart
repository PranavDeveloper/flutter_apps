import 'package:flutter/material.dart';

void main() {
  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DisplayImages(),
    );
  }
}

class DisplayImages extends StatefulWidget {
  const DisplayImages({Key? key}) : super(key: key);

  @override
  State createState() => _DisplayImagesState();
}

class _DisplayImagesState extends State<DisplayImages> {
  List<String> imagesList = [
    "https://cdn.pixabay.com/photo/2020/01/22/22/06/wyoming-4786394_640.jpg",
    "https://cdn.pixabay.com/photo/2021/03/11/02/57/mountain-6086083_640.jpg",
  
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          "List view build demo",
          style: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.w900,
          ),
        ),
      ),
      body: ListView.builder(
        itemCount: imagesList.length,
        itemBuilder: (context, index) {
          return Container(
            margin:const  EdgeInsets.all(10),
            child: Image.network(
              imagesList[index],
            ),
          );
        },
      ),
    );
  }
}
