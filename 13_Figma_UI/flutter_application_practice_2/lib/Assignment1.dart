

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
class Assignment1 extends StatefulWidget{
  const Assignment1({super.key});
  @override
  State  createState()=>_Assignment1State();
  
}
class _Assignment1State extends State{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Container(
        width: 500,
        decoration: const BoxDecoration(
          gradient: LinearGradient(begin: Alignment.topLeft,end: Alignment.center, colors: [
            Color.fromRGBO(197, 4, 98, 1),
            Color.fromRGBO(80, 3, 112, 1)
          ]
          )
        ),
         child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(padding: const EdgeInsets.only(left: 20,top: 80),
            child: Image.asset("assets/images/arrow-left.png",
            height: 26,
            width: 26,),
            ),
            
            Padding(padding: const EdgeInsets.only(left:38,top: 20,right: 38),
            child: Container(
                child: Column(
                  children: [
                     Text("UX Designer from Scratch.",
                     style:GoogleFonts.jost(
                       fontSize:32.61,
                       fontWeight: FontWeight.w500,
                       color:const Color.fromRGBO(255, 255, 255, 1)
                     ),
                     ),
                     const SizedBox(
                      height: 10,
                     ),
                     Text("Basic guideline & tips & tricks for how to become a UX designer easily.",
                     style: GoogleFonts.jost(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(228, 205, 225, 1) 
                     ),
                     ),
                    

                  ],

                ),
            ),
            ),
             Padding(padding: EdgeInsets.only(left:38,top:20,right: 50),
            child: Row(
              children: [
                SizedBox(
                  height: 34,
                  width: 34,
                  child: Image.asset("assets/images/Group 4912.png"),
                ),
                const SizedBox(
                  width: 10,
                ),
                Text("Author:",
                style: GoogleFonts.jost(
                   fontSize: 16,
                   fontWeight:FontWeight.w500,
                   color:const Color.fromRGBO(190, 154, 197,1 )
                ),

                ),
                Text("Jenny",
                style: GoogleFonts.jost(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color:const Color.fromRGBO(255, 255, 255, 1)
                ),
                ),
                const SizedBox(
                  width: 40,
                ),
                 Row(
                  children: [
                  
                     Text("4.8",
                     style:GoogleFonts.jost(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: const Color.fromRGBO(255, 255, 255, 1) 
                     ) 
                     ,),
                     SizedBox(
                      child: Image.asset("assets/images/Star 1.png"),
                    ),
                    const Text("(20 review)",
                    style:TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: Color.fromRGBO(190, 154, 197, 1)
                    ) ,)
                  ],
                ),
                
              ],
            ),
            
            ),
            const SizedBox(
                  height: 30,
                ),
          
          Expanded(
            child: Container(
              padding: const EdgeInsets.only(
                top:0,
                left: 35,
                right: 35
              ),
              decoration: const BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 1),
               borderRadius: BorderRadius.only(topLeft: Radius.circular(38),topRight: Radius.circular(38)) 
              ),
              child: ListView.builder(itemCount:5,itemBuilder:(context,index){
                  return Container(
                     width: 290,
                     height: 67, 
                     padding: const EdgeInsets.only(left:4,top:4,bottom:4),
                     margin: const EdgeInsets.only(bottom:20),
                     decoration:const  BoxDecoration(
                      color: Color.fromRGBO(255, 255, 255, 1),
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      boxShadow: [
                        BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.15),
                          offset: Offset(0,6 ),
                          blurRadius: 11,
                          spreadRadius: 0,
                        )
                      ]
                     ),
                     
                     child: Row(
                      children: [
                        Container(
                          height: 60,
                          width: 46,
                          decoration: BoxDecoration(
                            borderRadius:BorderRadius.circular(10),
                            color: const Color.fromRGBO(230, 239, 239, 1),
                          ),
                          child: Image.asset("assets/images/youtube.png"),
                          
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(
                              height: 5,
                            ),
                            Expanded(
                              child: Text("Introduction",
                              style: GoogleFonts.jost(
                                fontSize: 17,
                                fontWeight: FontWeight.w500
                              ),
                              ),
                            ),
                            Expanded(
                              child: Text("Lorem Ipsum is simply dummy text ...",
                              style: GoogleFonts.jost(
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                color:const Color.fromRGBO(143, 143, 143, 1)
                              ),
                              ),
                            )
                          
                          ],
                        )
                      ],
                     ),
                  );
              },
              ),
            ),
          ),
          
          ],
          
         ),
      ) ,
    );
  }
}