
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Assignment1 extends StatelessWidget{
  const Assignment1({super.key});
  @override
  Widget build(BuildContext context){
    return  Scaffold(
      backgroundColor: const Color.fromRGBO(205,218,218,1),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(padding: const EdgeInsets.only(top: 47,left: 20,right: 20),
          child: Row(
            children: [
              Image.asset("asset/image/menu.png"),
              const Spacer(
                flex: 1,
              ),
              Image.asset("asset/image/bell.png")
            ],
          ),
          ),
          
            
              Container(
                margin: const EdgeInsets.only(left: 20,top: 19,),
                child: Text("Welcome to New",
                style: GoogleFonts.jost(
                  fontSize: 26.98,
                  fontWeight: FontWeight.w300,
                  color:const Color.fromRGBO(0, 0, 0, 1)
                )
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 20),
                child:  Text("Educourse",
                
                style:GoogleFonts.jost(
                  fontSize: 37,
                  fontWeight: FontWeight.w800
                )
                ),
              ),
           Padding(padding:const  EdgeInsets.only(left: 20,top:15,bottom:29,right: 20),
          child: TextField(
            decoration: InputDecoration(hintText: "Enter your keyword",
            filled: true,
            fillColor: const Color.fromRGBO(255, 255, 255, 1),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(29),
            
            ),
            suffixIcon:Image.asset("asset/image/search.png"
            ) 
            ),
            ),
          ),
          Expanded(
            child: Container(
              
              
              decoration: const BoxDecoration(
               borderRadius:BorderRadius.only(topLeft: Radius.circular(38),topRight: Radius.circular(38)),
               color:Color.fromRGBO(255, 255, 255, 1)
              ),
              child:  Padding(padding: const EdgeInsets.only(left:20,top: 27,right: 15,bottom: 27),
                child:Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                     Text("Course For You",
                    style:GoogleFonts.jost(
                      fontSize: 18,
                      fontWeight: FontWeight.w500
                    )
                    ),
                   const SizedBox(
                    height: 15,
                   ),
                    SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        Container(
                          height: 242,
                          width: 190,
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                            gradient: LinearGradient(begin: Alignment.topCenter,end: Alignment.bottomCenter,
                            colors: [
                              Color.fromRGBO(197, 4, 98, 1),
                                Color.fromRGBO(80, 3, 112, 1)
                              
                            ]
                          ),
                        ),
                        child:Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Container(
                                height: 50,
                                padding: const EdgeInsets.only(left: 15,top:12),
                                child:  Text("UX Designer from Scratch.",
                                style:GoogleFonts.jost(
                                  fontSize: 17,
                                  fontWeight: FontWeight.w500,
                                  color: const Color.fromRGBO(255, 255, 255, 1)
                                )
                                )

                              ),
                            ),
                               Container(
                              padding:const EdgeInsets.only(left: 13,bottom:15),
                              child: Image.asset("asset/image/person.png"),
                               ),
                               
                          ],
                        ) ,
                      ),
                      const SizedBox(width: 15,),
                       Container(
                        padding: EdgeInsets.only(left: 35),
                          height: 242,
                          width: 190,
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(14)),
                            gradient: LinearGradient(begin: Alignment.topCenter,end: Alignment.bottomCenter,
                            colors: [
                              Color.fromRGBO(0, 77, 228, 1),
                                Color.fromRGBO(1, 47, 135, 1)
                              
                            ]
                          ),
                        ),
                        child:Column(
                          
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Container(
                                height: 50,
                                padding: const EdgeInsets.only(top:12,left: 5),
                                
                                child:  Text("Design Thinking The Beginner",
                                style:GoogleFonts.jost(
                                  fontSize: 17,
                                  fontWeight: FontWeight.w500,
                                  color: const Color.fromRGBO(255, 255, 255, 1)
                                )
                                )

                              ),
                            ),
                               Container(
                              padding:const EdgeInsets.only(left: 15,bottom:15),
                              child: Image.asset("asset/image/Objects.png"),
                               ),
                               
                          ],
                        ) ,
                      ),
                      

                      
                        
                      
                      ],
                    ),
                    
                   ),
                    const SizedBox(
                    height: 35,
                  ),
                  Container(
                    height: 26,
                    child: Text(
                      "Course By Category",
                      style: GoogleFonts.jost(
                        fontSize: 20,
                        fontWeight: FontWeight.w500,
                        color: const Color.fromRGBO(0, 0, 0, 1),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Image.asset("asset/image/C-1.png"),
                      Image.asset("asset/image/C-2.png"),
                      Image.asset("asset/image/C-3.png"),
                      Image.asset("asset/image/C-4.png")
                    ],
                  )

                  ],
                ) ,
              ),
            ),
            
          )
          
        ]
      ),
          
          
       
    );

   
 }
}